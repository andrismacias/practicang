import { Component } from '@angular/core';
import { ProductDetail } from '../../interfaces/productDetail.interface';

@Component({
  selector: 'app-tableproducts',
  templateUrl: './tableproducts.component.html'
})
export class TableproductsComponent {

    
  productsList: ProductDetail[] = [
    {
      idProducto: 1,
      producto: 'Pc Gammer',
      modelo: 'Asus XX33-24',
      precio: 452.89
    },
    {
      idProducto: 2,
      producto: 'Monitor UHD',
      modelo: '2PPLG',
      precio: 180.00
    }
];



  eventoEditar(){
  }


  eventoEliminar(){

  }
}
