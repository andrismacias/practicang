export interface ProductDetail{
    idProducto: number;
    producto: string;
    modelo: string;
    precio: number; 
    stock?: number;

}